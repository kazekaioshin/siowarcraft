package org.btssio.slam.siowarcraft;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;


public class OrcsFragment extends Fragment implements AdapterView.OnItemClickListener {
    MediaPlayer mp;
    String questions[] = {
            "Arrrgh", "Dapou", "Fracasser les", "Gloctare", "Groooah", "Guizmopanpan",
            "Hein", "Il craint le grand mechant loup", "Je ne suis pas kermite", "Je te tiens tu me tiens",
            "Le premier de nous deux", "Maitre", "Ma vie pour la horde", "Mourrut", "Occupez vous de votre armee",
            "Oui", "Pour la horde", "Quoi", "Quoi encore",
            "Skoibo", "Tuer", "Zagzag"
    };

    Integer[] IDSONS = {
            R.raw.argh,R.raw.dapou, R.raw.fracasserles,R.raw.gloctare,R.raw.groah,R.raw.guizmopanpan,
            R.raw.hein,R.raw.ilcraintlegrandmechantloup,R.raw.jenesuispaskermite,R.raw.jetetienstumetiens,R.raw.lepremierdenousdeux,
            R.raw.maitre,R.raw.maviepourlahorde,R.raw.mourut,R.raw.occupervousdevotrearmee,R.raw.oui,R.raw.pourlahorde,R.raw.quoi,
            R.raw.quoiencore,R.raw.skoibo,R.raw.tuer,R.raw.zagzag
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_orcs, container, false);
        ArrayAdapter adtr = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_expandable_list_item_1, questions);
        ListView lv = (ListView) v.findViewById(R.id.list);
        lv.setAdapter(adtr);
        lv.setOnItemClickListener(this);
        return v;
    }

    protected void playThatSound(int position) {

        if (mp != null) {
            mp.reset();
            mp.release();
        }
        mp = MediaPlayer.create(getActivity(),IDSONS[position]);
        mp.start();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        playThatSound(position);
    }
}
